package com.example.maheshprasad.sendstringtoanotheractivitythroughintent;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {

    TextView mtext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        //from intebt we can get data from any activity
        Intent intent = getIntent();

        mtext = findViewById(R.id.message_of_text);
        String messagetext = intent.getStringExtra("message");
        mtext.setText(messagetext);
    }
}
