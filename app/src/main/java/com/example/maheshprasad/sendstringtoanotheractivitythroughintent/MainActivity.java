package com.example.maheshprasad.sendstringtoanotheractivitythroughintent;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    EditText medt;
    Button mbtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        medt = findViewById(R.id.edt);
        mbtn = findViewById(R.id.sendbtn);
        mbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String sedt = medt.getText().toString();

                //From intetn we can send data to any activiy
                //this help to pass data in any activity
                Intent intent = new Intent(MainActivity.this, SecondActivity.class);
                intent.putExtra("message", sedt);
                startActivity(intent);
            }
        });
    }
}
